//[SECTION] Dependencies and Modules
  const exp = require("express");
  const controller = require('../controllers/courses');
  const auth = require('../auth');


  // destructure verify from auth
  const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
  const route = exp.Router(); 

//[SECTION] [POST] Routes
//Since this route should not be accessed by all users, we have to authorize only some logged in users who have the proper authority:
//First, verify if the token the user is using is legit.
//Second, if the authorized is admin only, add the verifyAdmin
   route.post('/create', verify, verifyAdmin, (req, res) => { 
       let data = req.body; 
       controller.createCourse(data).then(outcome => {
       	   res.send(outcome); 
       });
   });

//[SECTION] [GET] Routes
   route.get('/all', (req, res) => {
      controller.getAllCourse().then(outcome => {
        res.send(outcome);
      });
   });


// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending on the information provided in the url
   route.get('/:id', (req, res) => {
    // Since the course ID will be sent via the URL, we cannot retrieve it from the request body
    // We can however retrieve the course ID by accessing the request's "params" property which contains all the parameters provided via the url
        // Example: URL - http://localhost:4000/courses/613e926a82198824c8c4ce0e
        // The course Id is "613e926a82198824c8c4ce0e" which is passed via the url that corresponds to the "id" in the route
      let courseId = req.params.id; 
      controller.getCourse(courseId).then(result => { 
          res.send(result);
      }); 
   }); 

   route.get('/', (req, res) => {
      controller.getAllActiveCourse().then(outcome => {
         res.send(outcome); 
      });
   });

//[SECTION] [PUT] Routes
  route.put('/:id', verify, verifyAdmin, (req, res) => {
      let id = req.params.id; 
      let details = req.body;
      let cName = details.name; 
      let cDesc = details.description;
      let cCost = details.price;       
      if (cName  !== '' && cDesc !== '' && cCost !== '') {
        controller.updateCourse(id, details).then(outcome => {
            res.send(outcome); 
        });      
      } else {
        res.send({message:'Incorrect Input, Make sure details are complete'});
      }
  }); 

  route.put('/:id/archive', verify, verifyAdmin, (req, res) => {
      let courseId = req.params.id; 
      controller.deactivateCourse(courseId).then(resultOfTheFunction => {
          res.send(resultOfTheFunction);
      });
  });

//[SECTION] [DEL] Routes 
  route.delete('/:id', verify, verifyAdmin, (req, res) => {
     let id = req.params.id; 
     controller.deleteCourse(id).then(outcome => {
        res.send(outcome);
     });
  });

//[SECTION] Export Route System
  module.exports = route; 
